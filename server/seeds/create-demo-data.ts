import { Knex } from "knex";
import { hashPassword } from "../User/Password_JWT/pw_hash";


export async function seed(knex: Knex): Promise<void> {

    let hash_password = await hashPassword('123456')
    console.log(hash_password)
    // Deletes ALL existing entries
    // await knex("client_info").del();
    // await knex("hair_stylist_info").del();
    await knex("timeslot").del();

    // Inserts seed entries
    // await knex("client_info").insert([
    //     { id: 1, username: "test1client",password:hash_password,email:"123456@abc.com",phone:"12345678",gender:"M"},
    // ]);
    // await knex("hair_stylist_info").insert([
    //     { id: 1, username: "test1stylist",password:hash_password,email:"123456@abc.com",phone:"12345678",gender:"M",profile_pic:"",bio:"",location:"",rating:"2",service_tag:["",""],image:[""],district:""},
    // ]);
    await knex("timeslot").insert([
        {hair_stylist_info_id:4,date:"2022-07-22",time:"09:00:00",available:"true"},
        {hair_stylist_info_id:4,date:"2022-07-22",time:"09:30:00",available:"true"},
        {hair_stylist_info_id:4,date:"2022-07-22",time:"12:00:00",available:"false"},
        {hair_stylist_info_id:4,date:"2022-07-22",time:"16:00:00",available:"true"},
        {hair_stylist_info_id:4,date:"2022-07-22",time:"19:30:00",available:"false"},
        {hair_stylist_info_id:4,date:"2022-07-22",time:"20:30:00",available:"false"}
    ])
};
