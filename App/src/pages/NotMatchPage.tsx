import React from 'react';
import { useLocation } from 'react-router';
import styles from './NotMatchPage.module.scss';

export default function NotMatchPage() {
    const location = useLocation()
    return (
            <div className={styles.notMatch}>
                <h2>404 Not Found</h2>
                <div className={styles.message}>
                    <div>The content you trying to find does not exist.</div>
                    <div>You are at: &nbsp;<span>{location.pathname}</span></div>
                    {/* <div>{location.search}</div> */}
                </div>
            </div>
    )
}