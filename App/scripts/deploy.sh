set -e
set -x

source scripts/config

npm i
npm run build
//run 
aws s3 sync build s3://hang-haircut

aws cloudfront create-invalidation --distribution-id E16YKW3DYI6JAD --paths '/*'

echo Done
