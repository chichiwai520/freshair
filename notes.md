# FresHair

A web app which allows matching of hair stylists and users.

<br>

## Characteristics:

- Lists of hair stylists can be seen by Guests or Users, with their names, services and districts stated (**Searching** and **Filtering** should be allowed).
- Hair stylists can edit their bio, job details, previous work, locations, contact info in their profiles.
- Hair stylists can add and adjust hair styling plans in their profiles.
- `Calendar, timetable with available time slots of the stylists for clients to select. (Optional ?)`
- Clients can make requests for hair styling bookings, with type of service(s) specified.
- Hair stylists can accept or decline the requests from clients.
- Once the request is accepted, email would be sent to the clients.
- `Clients can rate or comment on the hair stylists' services. (Optional ?)`
- `Hair stylists can post their ideas and opinions on their blog. (Optional ?)`
- `Clients can see the nearby hair stylists with respect to their current location. (Optional ?)`

<br>

##  Work-flow:

**First Stage ():**
3 people for Frontend

**Seconds Stage ():**
1 person for Frontend 2 people for Backend (1 Person to make a set of standard CSS)

**Third Stage (2 - 3 days):**
Deployment of Frontend and Backend to server

<br>

## Elements Included:

<br>

### **Client Side App**
---

<br>

### **Guest**

> - Home Page:
>   - [ ] Prompt to Login

> - Login Page: 
>   - [ ] Login
>       - [ ] Login as Client
>       - [ ] Login as Hair Stylists
>   - [ ] Register
>       - [ ] Register as Client
>       - [ ] Register as Hair Stylists
>   - [ ] `Reset Password through email (Optional)`
>   - [ ] `Social Login (Optional)`

> - Search Hair Stylists:
>   - [ ] Search Bar for searching Hair Stylists or other keywords (districts, services)
>       - [ ] Filter to enhance the searching experience (services, districts, price range)
>   - [ ] List of Hair Stylists with Info with "Make a Booking Request" Button
>   - [ ] "Make A Booking Request" Button should redirect to Login Page

> - Booking Page:
>   - [ ] Redirect to Login Page
 
> - Profile: 
>   - [ ] Prompt to Login
>   - [ ] "Settings"
>   - [ ] "Terms of Service"

<br>

### **Logged In User**

> - Home Page:
>   - [ ] Greetings
>   - [ ] `Nearby Hair Stylists (Optional)`
>   - [ ] `Hair Stylists' Posts from their Blog (Optional)`
>   - [ ] Reminder for Upcoming Booking
>       - [ ] "More" Button for more info of Upcoming Booking
>       - [ ] State "Currently No Bookings have been made", if no bookings made
 
> > - `Blog Page: (Optional)`
> >   - [ ] `List of Posts or Articles made by the Hair Stylists previously with a click effect to get into the specific article`
> 
> > - `Article Page: (Optional)`
> >   - [ ] `Show the whole article selected from the list in Blog Page`

> - Search Hair Stylists:
>   - [ ] Search Bar for searching Hair Stylists or other keywords (districts, services)
>       - [ ] Filter to enhance the searching experience (services, districts, price range)
>   - [ ] List of Hair Stylists with Info with "Details and Booking" Button
>   - [ ] "Details and Booking" Button to redirect to Hair Stylist's Page

> - Hair Stylist's Page:
>   - [ ] Hair Stylist's job details, previous work, locations, contact info
>   - [ ] Hair Stylist's hair styling plans
>   - [ ] "Make A Booking Request" Button to redirect to Booking Page
>   - [ ] `"Write A Review" button to rate and comment on the service of hair stylist (Optional)`

> - Bookings Page: `(Booking System or simply Making Request ?)`
>   - [ ] Box / List with Requests and Booked Session Info and "Edit Appointment" Button
>   - [ ] "Edit Booking" Button should redirect to Booking Page
>   - [ ] `Or simply Box / List with Requests and Booked Session Info and "Cancel" Button ?`
>   - [ ] `Send an email and notification to the hair stylists once the Booking is cancelled ?`
 
> - Make New Booking Page (only accessible from "Bookings Page" and "Edit Appointment" Button):
>   - [ ] Select Boxes and Input Boxes for Making New Booking (Be sure the Bookings won't be overlapped)
>   - [ ] `Online Calender Booking System (with Time Slots) (Optional)`
>   - [ ] "Request" Button to send request to hair stylists

> - Settings: 
>   - [ ] `"App Settings" (Optional)`
>   - [ ] "Account Information", (including name, gender, tel, address?, email?)
>   - [ ] "Previous Records" to allow Client check the previous bookings
>   - [ ] "Terms of Service"

<br>

### **Hair Stylist**
---
> - Home Page:
>   - [ ] Boxes to show Total Bookings, Upcoming Bookings, Completed Bookings (Optional)
>   - [ ] List of Bookings Today

> > - `Blog Page: (Optional)`
> >   - [ ] `List of Posts or Articles made by the Hair Stylists previously with "Edit" Button to redirect to the Writing Page`
> >   - [ ] `"Publish New Article" Button to redirect to Writing Page`
> 
> > - `Writing Page (Optional)`
> >   - [ ] Text Box for posting Hair Stylists' ideas and opinions and "Publish" Button

> - `Schedule Page: (Optional)`
>   - [ ] `Calendar of Booked Consultations with Time`
>   - [ ] `With Function to set AVAILABLE, UNAVAILABLE or OCCUPIED in specific Time`

> - Bookings Page: `(Booking System or simply Making Request ?)`
>   - [ ] Box / List with Requests and Booked Session Info and "Edit Appointment" Button
>   - [ ] "Edit Booking" Button should redirect to Booking Page
>   - [ ] `Or simply Box / List with Requests and Booked Session Info and "Cancel" Button, "Accept" Button, "Reject" Button ?`
>   - [ ] `Send an email and notification to the clients once the Booking is confirmed ?`

> - Settings: 
>   - [ ] `"App Settings" (Optional)`
>   - [ ] "Account Information", (including name, gender, tel, address?, email?)
>   - [ ] "Job Details and Services" OR "Profile" to edit their bio, job details, previous work, locations, contact info, etc
>   - [ ] "Previous Records" to allow Hair Stylists check the previous bookings
>   - [ ] "Terms of Service"


<br>

# Important Notes

**About the Flow of Requesting for/ Making a Booking:**

> **Clients** should be able to see available sessions set by **Hair Stylists** and make requests for bookings in those time slots.

<br>

> If one **Client** wants to makes a request for a session `from 15:00 to 17:00 on Sunday,` he/ she should press the "Make a Booking Request" button.

<br>

> Request will be sent to the **Hair Stylist** and they can contact the **Client** through the contact ways provided (phones calls or messages).

<br>

> If the **Hair Stylist** and **Client** reach an agreement, the **Hair Stylist** should set the `15:00 - 17:00 session on Sunday` as `OCCUPIED`. 
> 
> So other clients should see the updated timetable and would NOT be able to make any request for session `between 15:00 and 17:00 on Sunday`.

<br>

> If the **Client** or the **Hair Stylist** wish to `CANCEL` a session, one should inform the other party `(before a specified time ?)`.
>
> Then the session `15:00 - 17:00 on Sunday` should be set as `AVAILABLE` by the **Hair Stylist**.
> 
> The timetable will be updated and other **Clients** should be able to make requests for a session during that period if they want to.

<br>

> If the **Client** wants to rate the **Hair Stylist** after the service. He/ she can go to the Hair Stylist's Page to "Write a Review". (Optional)

<br>

# Design

> Using **Barber's Pole**, which can always be seen outside a **Salon** as the element of the application.
> 
> Therefore, the App could be in **`Retro`** style and adopt the colours of **Barber's Pole**.
> 
> ![barbers-pole](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSk663tJQw7BggoGUbA2vvnvzehP8yHFHusoQ&usqp=CAU)

<br>

### **Icon of App:**

E.g.

![barbers-pole-icon-example](https://image.shutterstock.com/image-illustration/barber-shop-pole-isolated-on-260nw-519850228.jpg)

- `Undecided`

<br>

> Possible Choices (free stock photos):
>
> - [ ] https://cdn-icons-png.flaticon.com/512/802/802791.png?w=1060
> - [ ] https://cdn-icons-png.flaticon.com/512/1122/1122794.png?w=1060
> - [ ] https://cdn-icons-png.flaticon.com/512/863/863499.png?w=1060&t=st=1657265544~exp=1657266144~hmac=b069469a6f9cf01f7e9935267dcc1aeb43e5dd4310a8a0590b72e5bd0a78101e
>  - [ ] https://cdn-icons-png.flaticon.com/512/1122/1122825.png?w=1060&t=st=1657265547~exp=1657266147~hmac=bcbb0aabca0076e2c67b02696f6c6c375ab440ff048d73b608602c0f771229d0


<br>

### **Fonts:**

- App Title: `Undecided`

- Content: `Undecided`

> Possible Choices:
> 
> - [ ] https://www.cdnfonts.com/barber-shop.font (`for Content ?`)
> - [ ] https://www.cdnfonts.com/alvaro.font
> - [ ] https://www.cdnfonts.com/joaquin.font
> - [ ] https://www.cdnfonts.com/klick.font
> - [ ] https://www.cdnfonts.com/deltha.font


<br>

### **Colours:**

![barberspole-colour](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiDoJzK26FC_-4T3hpYX9q5I0XfxYyPgqU4g&usqp=CAU)

- Blue
- Red
- White
- Black

<br>

### **Libraries:**

- Base: Mantine UI
- `Calendar: Undecided`